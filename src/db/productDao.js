import executeQuery from "./db.js";
import queries from "./queries.js";

const findAll = async () => {
  console.log("..fetching all products from db");
  const result = await executeQuery(queries.findAll);
  return result;
};

const insertProduct = async product => {
  const { name, price } = product;
  const params = [ name, price ];
  const result = await executeQuery(queries.insertProduct, params);
  console.log(`product ${name} inserted succesfully`);
  return result;
};

const findOne = async id => {
  const result = await executeQuery(queries.findOne, [id]);
  console.log(`found ${result.rows.length} products`);
  return result;
};

const updateProduct = async product => {
  const { id, name, price } = product;
  const params = [ id, name, price ];
  const result = await executeQuery(queries.updateProduct, params);
  return result;
};

const deleteOne = async id => {
  const result = await executeQuery(queries.deleteOne, [id]);
  return result;
};
 
export default {
  findAll,
  insertProduct,
  findOne,
  updateProduct,
  deleteOne,
};