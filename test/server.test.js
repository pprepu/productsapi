import { jest } from "@jest/globals";
import { pool } from "../src/db/db.js";
import server from "../src/server.js";
import request from "supertest";

describe("GET /products", () => {
  const mockResponse = {
    rows: [
      { id: 1, name: "testProduct", price: 25 },
      { id: 2, name: "banana", price: 10 }
    ]
  };

  beforeEach(() => {
    pool.connect = jest.fn(() => {
      return {
        query: () => mockResponse,
        release: () => null
      };
    });
  });

  it("returns 200 with all the products", async () => {
    const response = await request(server).get("/products");
    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockResponse.rows);
  });
});

describe("POST /products", () => {
  const mockResponse = {
    rows: [
      { id: 1, name: "banana", price: 10 }
    ]
  };

  beforeEach(() => {
    pool.connect = jest.fn(() => {
      return {
        query: () => mockResponse,
        release: () => null
      };
    });
  });

  it("returns 201 with the added product if all fields are sent", async () => {
    const newProduct = {
      name: "banana",
      price: 10
    };
    const response = await request(server).post("/products").send(newProduct);
    expect(response.status).toBe(201);
    expect(response.body).toEqual(mockResponse.rows[0]);
  });

  it("returns 403 with an error message if 'name' field is missing", async () => {
    const newProduct = {
      price: 10
    };
    const response = await request(server).post("/products").send(newProduct);
    expect(response.status).toBe(403);
    expect(response.text).toBe("name or price not sent");
  });

  it("returns 403 with an error message if 'price' field is missing", async () => {
    const newProduct = {
      name: "banana"
    };
    const response = await request(server).post("/products").send(newProduct);
    expect(response.status).toBe(403);
    expect(response.text).toBe("name or price not sent");
  });
});

describe("GET /products/id", () => {
  const mockResponse = {
    rows: [
      { id: 1, name: "banana", price: 10 }
    ]
  };

  beforeEach(() => {
    pool.connect = jest.fn(() => {
      return {
        query: () => mockResponse,
        release: () => null
      };
    });
  });

  it("returns 200 and the product", async () => {
    const response = await request(server).get("/products/1");
    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockResponse.rows[0]);
  });
});

describe("PUT /products/id", () => {
  beforeEach(() => {
    pool.connect = jest.fn(() => {
      return {
        query: () => null,
        release: () => null
      };
    });
  });

  it("returns 204 with an empty response", async () => {
    const updatedProduct = {
      name: "banana",
      price: 10
    };
    const response = await request(server).put("/products/1").send(updatedProduct);
    expect(response.status).toBe(204);
    expect(response.text).toBe("");
  });
});

describe("DELETE /products/id", () => {
  beforeEach(() => {
    pool.connect = jest.fn(() => {
      return {
        query: () => null,
        release: () => null
      };
    });
  });

  it("returns 204 with an empty response", async () => {
    const response = await request(server).delete("/products/1");
    expect(response.status).toBe(204);
    expect(response.text).toBe("");
  });
});